//
//  main.m
//  MKImageGallery
//
//  Created by Rolf Koczorek on 14.07.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
