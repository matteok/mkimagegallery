//
//  AppDelegate.h
//  MKImageGallery
//
//  Created by Rolf Koczorek on 14.07.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
