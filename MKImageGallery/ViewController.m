//
//  ViewController.m
//  MKImageGallery
//
//  Created by Rolf Koczorek on 14.07.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) MKImageGalleryView *imageGalleryView;
@property (nonatomic) NSInteger imageAmount;
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, strong) NSString *caption;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.imageAmount = 2;
    self.imageName = @"turtle.jpg";
    self.caption = @"Schildkröte";
    self.imageGalleryView = [[MKImageGalleryView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-200)];
    [self.view addSubview:self.imageGalleryView];
    self.imageGalleryView.dataSource = self;
   self.imageGalleryView.title = @"Als die Tiere den Wald verliessen.";
    
	// Do any additional setup after loading the view, typically from a nib.
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MKImageGalleryView data source

-(NSInteger) numberOfElementsInImageGalleryView:(MKImageGalleryView *)imageGalleryView
{
    return self.imageAmount;
}

-(UIImage*) imageForIndex:(NSInteger)index inImageGalleryView:(MKImageGalleryView *)imageGalleryView
{
    return [UIImage imageNamed:@"turtle.jpg"];
}

-(NSString*) captionForIndex:(NSInteger)index inImageGalleryView:(MKImageGalleryView *)imageGalleryView
{
    return self.caption;
}

-(UIImage*) thumbnailForIndex:(NSInteger)index inImageGalleryView:(MKImageGalleryView *)imageGalleryView
{
    return [UIImage imageNamed:@"turtle.jpg"];
}

@end
