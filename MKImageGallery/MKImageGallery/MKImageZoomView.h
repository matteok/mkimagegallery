//
//  ImageZoomView.h
//  COSYNA
//
//  Created by Matteo Koczorek on 05.02.13.
//  Copyright (c) 2013 Matteo Koczorek. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MKImageGalleryScrollView;

@interface MKImageZoomView : UIView <UIScrollViewDelegate>


@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) MKImageGalleryScrollView *scrollView;
@property (nonatomic, strong) UIView *contentView;


-(id) initWithFrame:(CGRect)frame andImage:(UIImage*) image;

-(void) zoomOut;

@end
