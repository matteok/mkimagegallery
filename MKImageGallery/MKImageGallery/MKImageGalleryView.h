//
//  MKImageGalleryView.h
//  GesturizedImageGallery
//
//  Created by Matteo Koczorek on 19.01.14.
//  Copyright (c) 2014 Matteo Koczorek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MKImageGalleryFullscreenView.h"


@class MKImageGalleryFullscreenView;
@class MKImageGalleryView;


//delegate protocol
@protocol MKImageGalleryViewDelegate <UIScrollViewDelegate>

@optional

-(void) imageGalleryView: (MKImageGalleryView*) imageGalleryView willShowFullscreenImageWithIndex: (int) index;
-(void) imageGalleryView: (MKImageGalleryView*) imageGalleryView willHideFullscreenImageWithIndex: (int) index;
-(void) imageGalleryView:(MKImageGalleryView *)imageGalleryView fullscreenView: (MKImageGalleryFullscreenView*) fullscreenView scrollViewDidScroll:(UIScrollView*) scrollView;
@end

//data source protocol
@protocol MKImageGalleryViewDataSource <NSObject>

- (NSInteger)numberOfElementsInImageGalleryView:(MKImageGalleryView *)imageGalleryView;
- (UIImage*)imageForIndex: (NSInteger) index inImageGalleryView:(MKImageGalleryView *)imageGalleryView;
- (UIImage*)thumbnailForIndex: (NSInteger) index inImageGalleryView:(MKImageGalleryView *)imageGalleryView;
- (NSString*)captionForIndex: (NSInteger) index inImageGalleryView:(MKImageGalleryView *)imageGalleryView;

@end


@interface MKImageGalleryView : UIScrollView <MKImageGalleryFullscreenViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSMutableArray *images;
@property (nonatomic, strong) NSMutableArray *thumbnails;
@property (nonatomic, strong) NSMutableArray *captions;
@property (nonatomic, strong) NSMutableArray *captionViews;
@property (nonatomic, strong) NSMutableArray *buttons;

@property (nonatomic, weak) id<MKImageGalleryViewDelegate> delegate;
@property (nonatomic, weak) id<MKImageGalleryViewDataSource> dataSource;

@property (nonatomic, weak) UIView *mainView;
@property (nonatomic, strong) MKImageGalleryFullscreenView *fullscreenView;

@property (nonatomic, strong) UIPinchGestureRecognizer *pinchRecognizer;
@property (nonatomic, strong) UIRotationGestureRecognizer *rotationRecognizer;
@property (nonatomic, strong) UIPinchGestureRecognizer *fullscreenPinchRecognizer;
@property (nonatomic, strong) UIRotationGestureRecognizer *fullscreenRotationRecognizer;

@property (nonatomic) CGFloat borderWidth;
@property (nonatomic, strong) UIColor *borderColor;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic) MKImageGalleryMode mode;

-(void) addImage: (UIImage*) image withThumbnail: (UIImage*) thumbnail andCaption:(NSString*) caption;
-(void) addImages: (NSArray*) images withThumbnails: (NSArray*) thumbnails andCaptions: (NSArray*) captions;

-(void) showFullscreenImageWithIndex:(int) index animated: (BOOL) animated;
-(void) hideFullscreenImageWithIndex:(int) index animated: (BOOL) animated;

-(void) reloadData;

@end
