//
//  MKImageGalleryScrollView.h
//  küste im blick
//
//  Created by Matteo Koczorek on 09.04.14.
//  Copyright (c) 2014 Kite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MKImageGalleryScrollView : UIScrollView <UIGestureRecognizerDelegate>

@end
