//
//  MKImageGalleryView.m
//  GesturizedImageGallery
//
//  Created by Matteo Koczorek on 19.01.14.
//  Copyright (c) 2014 Matteo Koczorek. All rights reserved.
// #

#define ANIMATION_DURATION 0.3f
#define PADDING_TOP 64
#define PADDING_LEFT 64
#define TITLE_PADDING_BOTTOM 84

#import "MKImageGalleryView.h"
#import "MKImageZoomView.h"
#import "MKImageGalleryScrollView.h"
#import <QuartzCore/QuartzCore.h>

@interface MKImageGalleryView()

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIButton *currentGestureTarget;
@property (nonatomic) BOOL shouldProcessPinchInFullscreen;
@property (nonatomic) CGPoint imageViewStartCenter;
@property (nonatomic) CGPoint pinchStartLocation;
@property (nonatomic) CGFloat pinchStartScale;
@property (nonatomic) NSUInteger numberOfImages;


@end

@implementation MKImageGalleryView


@synthesize mainView = _mainView;
@synthesize dataSource = _dataSource;
@synthesize numberOfImages = _numberOfImages;
@synthesize title = _title;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self construct];
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self construct];
    }
    return self;
}

-(void) construct
{
    //standard attributes
    self.borderColor = [UIColor whiteColor];
    self.borderWidth = 7;
    
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, TITLE_PADDING_BOTTOM, self.frame.size.width, self.frame.size.height)];
    [self addSubview:self.contentView];
    
    self.titleLabel = [[UILabel alloc] init];
    [self.titleLabel setFont:[UIFont systemFontOfSize:18]];
    [self.titleLabel setTextColor:[UIColor whiteColor]];
    
    [self addSubview:self.titleLabel];
    
    self.mode = MKImageGalleryModeVertical;
    self.images = [[NSMutableArray alloc] init];
    self.thumbnails = [[NSMutableArray alloc] init];
    self.buttons = [[NSMutableArray alloc] init];
    self.captions = [[NSMutableArray alloc] init];
    self.captionViews = [[NSMutableArray alloc] init];
    
    self.pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(gestureRecognized:)];
    [self addGestureRecognizer:self.pinchRecognizer];
    self.pinchRecognizer.delegate = self;
    
    self.rotationRecognizer = [[UIRotationGestureRecognizer alloc] init];
   [self addGestureRecognizer:self.rotationRecognizer];
    self.rotationRecognizer.delegate = self;
    
    self.fullscreenPinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchRecognizedInFullscreenView:)];
    self.fullscreenPinchRecognizer.delegate = self;
    
    self.fullscreenRotationRecognizer = [[UIRotationGestureRecognizer alloc] init];
    self.fullscreenRotationRecognizer.delegate = self;
    
    self.contentSize = CGSizeMake(700, 1800);
}

-(void) refreshContent
{
    [self clearData];
    for(int i = 0; i < (int) self.numberOfImages; i++) {
        if(self.dataSource && [self.dataSource respondsToSelector:@selector(imageForIndex:inImageGalleryView:)]) {
            UIImage *image = [self.dataSource imageForIndex:i inImageGalleryView:self];
            NSString *caption = @"";
            UIImage *thumbnail = image;
            
            if(self.dataSource && [self.dataSource respondsToSelector:@selector(thumbnailForIndex:inImageGalleryView:)]) {
                thumbnail = [self.dataSource thumbnailForIndex:i inImageGalleryView:self];
            }
            
            if(self.dataSource && [self.dataSource respondsToSelector:@selector(captionForIndex:inImageGalleryView:)]) {
                caption = [self.dataSource captionForIndex:i inImageGalleryView:self];
            }
            
            [self addImage:image withThumbnail:thumbnail andCaption:caption];
        }
    }
}

-(BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return YES;
}

-(BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return ([gestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]] || [gestureRecognizer isKindOfClass:[UIRotationGestureRecognizer class]]) && ([otherGestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]] || [otherGestureRecognizer isKindOfClass:[UIRotationGestureRecognizer class]]);
}

-(void) pinchRecognizedInFullscreenView: (UIPinchGestureRecognizer*) sender
{
    
    MKImageZoomView *imageZoomView = [self.fullscreenView.imageZoomViews objectAtIndex:self.fullscreenView.currentPage];
    if(sender.state == UIGestureRecognizerStateBegan) {
        self.imageViewStartCenter = imageZoomView.imageView.center;
        self.pinchStartLocation = [self.fullscreenPinchRecognizer locationInView:self.fullscreenView];
        if(sender.velocity > 0 || sender.scale > 1) {
            self.shouldProcessPinchInFullscreen = NO;
        }
        else {
            self.shouldProcessPinchInFullscreen = YES;
        }
    }
    if(!imageZoomView.scrollView.bouncesZoom && self.shouldProcessPinchInFullscreen) {
        imageZoomView.scrollView.maximumZoomScale = 1;
            UIButton *button = [self buttonWithIndex:self.fullscreenView.currentPage];
            CGPoint origin = [button.superview convertPoint:button.frame.origin toView:self.mainView];
        if(sender.state == UIGestureRecognizerStateBegan) {
            self.currentGestureTarget.hidden = NO;
            self.currentGestureTarget = button;
            self.currentGestureTarget.hidden = YES;
            [self copyProperties:button targetView:imageZoomView.imageView scale:1];
        }
        
        NSLog(@"pinch: %f", self.fullscreenPinchRecognizer.scale);
        CGPoint location = [self.fullscreenPinchRecognizer locationInView:self.fullscreenView];
        CGPoint relativeCenter = [imageZoomView convertPoint:self.imageViewStartCenter toView:self.mainView];
        CGPoint center = CGPointMake(relativeCenter.x + (location.x - self.pinchStartLocation.x), relativeCenter.y + (location.y - self.pinchStartLocation.y));
        
        if(self.fullscreenPinchRecognizer.numberOfTouches > 1) {
            [self setProperiesForImageViewWithSourceView:button targetView:imageZoomView scale:self.fullscreenPinchRecognizer.scale rotation:self.fullscreenRotationRecognizer.rotation center:center animated:NO completion:nil];
        }
        
        CGFloat ratioF = button.frame.size.width/imageZoomView.frame.size.width;
        if(sender.state == UIGestureRecognizerStateEnded) {
            if(sender.velocity < 0) {
                UIButton *currentGestureTarget = self.currentGestureTarget;
                MKImageGalleryView *me = self;
                [self setProperiesForImageViewWithSourceView:button targetView:imageZoomView scale:0  rotation:0 center:[button.superview convertPoint:button.center toView:self.mainView] animated:YES completion:^{
                    me.currentGestureTarget.hidden = NO;
                    [me.fullscreenView removeFromSuperview];
                    me.fullscreenView = nil;
                }];
            }
            else {
                CGPoint finalCenter = CGPointMake(imageZoomView.frame.size.width/2, imageZoomView.frame.size.height/2);
                [self setProperiesForImageViewWithSourceView:self.currentGestureTarget targetView:imageZoomView scale:1 rotation:0 center:finalCenter animated:YES completion:^{
                    imageZoomView.imageView.transform = CGAffineTransformIdentity;
                    imageZoomView.imageView.frame = CGRectMake(0, 0, imageZoomView.frame.size.width, imageZoomView.frame.size.height);
                }];
            }
        }
        
        CGRect rect = CGRectMake(origin.x, origin.y, button.frame.size.width, button.frame.size.height);
        CGAffineTransform rotation = CGAffineTransformMakeRotation(self.fullscreenRotationRecognizer.rotation);
        
        
        CGSize ratio = CGSizeMake(rect.size.width / imageZoomView.frame.size.width, rect.size.height / imageZoomView.frame.size.height);
        
        CGSize scaleValue = CGSizeMake(ratio.width, ratio.height);
        
        CGAffineTransform scale = CGAffineTransformMakeScale(scaleValue.width + ((1- scaleValue.width) * self.fullscreenPinchRecognizer.scale), scaleValue.height + ((1- scaleValue.height) * self.fullscreenPinchRecognizer.scale));
        
        CGAffineTransform transform = CGAffineTransformConcat(scale, rotation);
        
        //CGPoint location = [self.fullscreenPinchRecognizer locationInView:self.fullscreenView];
        
        /*if(self.fullscreenPinchRecognizer.numberOfTouches > 1) {
            imageZoomView.imageView.transform = transform;
            CGPoint relativeCenter = [imageZoomView convertPoint:self.imageViewStartCenter toView:self.mainView];
            imageZoomView.imageView.center = CGPointMake(relativeCenter.x + (location.x - self.pinchStartLocation.x), relativeCenter.y + (location.y - self.pinchStartLocation.y));
            
            CGFloat alpha = 1 - (1 - sender.scale);
            [self.fullscreenView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:alpha]];
            self.fullscreenView.closeButton.alpha = alpha;
        }*/
        
          /*  if(sender.state == UIGestureRecognizerStateEnded) {
                if(sender.velocity < 0) {
                    [UIView animateWithDuration:0.5f animations:^{
                        imageZoomView.imageView.center = [button.superview convertPoint:button.center toView:self.mainView];
                        imageZoomView.imageView.transform = CGAffineTransformMakeScale(ratio.width,ratio.height);
                        [self.fullscreenView setBackgroundColor:[UIColor clearColor]];
                        self.fullscreenView.closeButton.alpha = 0;
                    } completion:^(BOOL finished) {
                        self.currentGestureTarget.hidden = NO;
                        [self.fullscreenView removeFromSuperview];
                        self.fullscreenView = nil;
                    }];
                }
                else {
                    [UIView animateWithDuration:0.5f animations:^{
                        imageZoomView.imageView.center = self.imageViewStartCenter;
                        imageZoomView.imageView.transform = CGAffineTransformIdentity;
                        [self.fullscreenView setBackgroundColor:[UIColor blackColor]];
                        self.fullscreenView.closeButton.alpha = 1;
                    } completion:^(BOOL finished) {
                    }];
                }
                self.shouldProcessPinchInFullscreen = YES;
                imageZoomView.scrollView.maximumZoomScale = 4;
                //imageZoomView.scrollView.userInteractionEnabled = YES;
            }*/
        }
}

-(void) copyProperties: (UIView*) sourceView targetView: (UIView*) targetView scale: (CGFloat) scale
{
    scale = scale ? scale : 1;
    
    targetView.layer.borderWidth = self.borderWidth * scale;
    targetView.layer.borderColor = [self.borderColor CGColor];
    
    targetView.layer.shadowColor = sourceView.layer.shadowColor;
    targetView.layer.shadowOpacity = sourceView.layer.shadowOpacity;
    targetView.layer.shadowRadius = sourceView.layer.shadowRadius*scale;
    targetView.layer.shadowOffset = CGSizeMake(sourceView.layer.shadowOffset.width*scale, sourceView.layer.shadowOffset.height*scale);
    [targetView.layer setShadowPath:[[UIBezierPath
                                                   bezierPathWithRect:targetView.bounds] CGPath]];
    
}

-(void) setProperiesForImageViewWithSourceView: (UIView*) sourceView targetView:(MKImageZoomView*) targetView scale: (CGFloat) scale rotation: (CGFloat) rotation center:(CGPoint) center animated: (BOOL) animated completion:(void (^)())completion
{
    CGPoint origin = [sourceView.superview convertPoint:sourceView.frame.origin toView:self.mainView];
    CGRect rect = CGRectMake(origin.x, origin.y, sourceView.frame.size.width, sourceView.frame.size.height);
    
    
    CGAffineTransform rotationTransform = CGAffineTransformMakeRotation(rotation);
    CGSize ratio = CGSizeMake(rect.size.width / targetView.frame.size.width, rect.size.height / targetView.frame.size.height);
    
    CGAffineTransform scaleTransform = CGAffineTransformMakeScale(ratio.width + ((1- ratio.width) * scale), ratio.height + ((1- ratio.height) * scale));
    
    CGAffineTransform transform = CGAffineTransformConcat(scaleTransform, rotationTransform);
    
    CGFloat alpha = 1 - (1 - scale);
    
    MKImageGalleryFullscreenView *fcv = self.fullscreenView;
    
    void(^block)(void) = ^void() {
        targetView.imageView.center = center;
        targetView.imageView.transform = transform;
        [fcv setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:alpha]];
    };
    
    void(^_completion)(void) = ^void() {
        
        if(completion) completion();
    };
    
    if(animated) {
        //uiview animations
        [UIView animateWithDuration:ANIMATION_DURATION delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            block();
        }completion:^(BOOL completed){

            _completion();
        }];
        
        //layer animations
        CABasicAnimation *borderAlpha = [CABasicAnimation animationWithKeyPath:@"borderColor"];
        // animate from red to blue border ...
        borderAlpha.fromValue = (id)targetView.imageView.layer.borderColor;
        borderAlpha.toValue   = (id)[[self.borderColor colorWithAlphaComponent:1-alpha] CGColor];
        borderAlpha.duration = ANIMATION_DURATION;
        borderAlpha.repeatCount = 1;
        [borderAlpha setAutoreverses:NO];
        borderAlpha.removedOnCompletion = NO;
        borderAlpha.fillMode = kCAFillModeForwards;
        [targetView.imageView.layer addAnimation:borderAlpha forKey:@"borderColor"];
    }
    else {
        block();
        targetView.imageView.layer.borderColor = [[self.borderColor colorWithAlphaComponent:1-alpha] CGColor];
        _completion();
    }
    
    
}

-(void) gestureRecognized: (UIPinchGestureRecognizer*) sender
{
    //what if rotate happens but not pinch !!!
    if(sender.state == UIGestureRecognizerStateBegan) {
        CGPoint center = [sender locationInView:self];
        UIButton *button = [self buttonAtLocation:center];
        self.currentGestureTarget = button;
        self.currentGestureTarget.hidden = YES;
        if(self.currentGestureTarget) {
            [self addFullscreenView];
        }
    }
    if(self.currentGestureTarget) {
        int index = (int) self.currentGestureTarget.tag;
        MKImageZoomView *imageZoomView = [self.fullscreenView.imageZoomViews objectAtIndex:index];
        if(sender.state == UIGestureRecognizerStateBegan) {
            self.pinchStartLocation = [sender locationInView:self.fullscreenView];
            self.imageViewStartCenter = [self.currentGestureTarget.superview convertPoint:self.currentGestureTarget.center toView:self.mainView];
            
            
            CGFloat ratio = imageZoomView.frame.size.width / self.currentGestureTarget.frame.size.width;
            [self copyProperties:self.currentGestureTarget targetView:imageZoomView.imageView scale:ratio];
            
        }
        UIPinchGestureRecognizer *pinchRecognizer = (UIPinchGestureRecognizer*) sender;
        self.fullscreenView.currentPage = index;
        
        
        CGPoint location = [sender locationInView:self.fullscreenView];
        CGPoint center = CGPointMake(self.imageViewStartCenter.x + (location.x - self.pinchStartLocation.x), self.imageViewStartCenter.y + (location.y - self.pinchStartLocation.y));
        
       if(sender.numberOfTouches > 1) {
            [self setProperiesForImageViewWithSourceView:self.currentGestureTarget targetView:imageZoomView scale:pinchRecognizer.scale-1 rotation:self.rotationRecognizer.rotation center:center animated:NO completion:nil];
        }
        if(pinchRecognizer.state == UIGestureRecognizerStateEnded) {
            CGPoint finalCenter = CGPointMake(imageZoomView.frame.size.width/2, imageZoomView.frame.size.height/2);
            MKImageGalleryView *me = self;
            if(pinchRecognizer.velocity >= 0) {
                [self setProperiesForImageViewWithSourceView:self.currentGestureTarget targetView:imageZoomView scale:1 rotation:0 center:finalCenter animated:YES completion:^{
                    imageZoomView.imageView.transform = CGAffineTransformIdentity;
                    imageZoomView.imageView.frame = CGRectMake(0, 0, imageZoomView.frame.size.width, imageZoomView.frame.size.height);
                }];
            }
            else {
                [self setProperiesForImageViewWithSourceView:self.currentGestureTarget targetView:imageZoomView scale:0 rotation:0 center:self.imageViewStartCenter animated:YES completion:^{
                    me.currentGestureTarget.hidden = NO;
                    [me.fullscreenView removeFromSuperview];
                    me.fullscreenView = nil;
                }];
            }
        }
        /*
        CGPoint origin = [self.currentGestureTarget.superview convertPoint:self.currentGestureTarget.frame.origin toView:self.mainView];
        CGRect rect = CGRectMake(origin.x, origin.y, self.currentGestureTarget.frame.size.width, self.currentGestureTarget.frame.size.height);
        
        CGPoint location = [sender locationInView:self.fullscreenView];
        
        //CGFloat mod = pinchRecognizer.scale;
        self.fullscreenView.currentPage = index;
        
        CGAffineTransform rotation = CGAffineTransformMakeRotation(self.rotationRecognizer.rotation);
        CGSize ratio = CGSizeMake(rect.size.width / imageZoomView.frame.size.width, rect.size.height / imageZoomView.frame.size.height);
        CGAffineTransform scale = CGAffineTransformMakeScale(ratio.width*pinchRecognizer.scale, ratio.height*pinchRecognizer.scale + (ratio.width-ratio.height)*(pinchRecognizer.scale-1));
        
        CGAffineTransform transform = CGAffineTransformConcat(scale, rotation);
        
        if(sender.numberOfTouches > 1) {
            
            imageZoomView.imageView.center = CGPointMake(self.imageViewStartCenter.x + (location.x - self.pinchStartLocation.x), self.imageViewStartCenter.y + (location.y - self.pinchStartLocation.y));
            
            imageZoomView.imageView.transform = transform;
            
            CGFloat alpha = (sender.scale-1)/2.3;
            [self.fullscreenView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:alpha]];
            self.fullscreenView.closeButton.alpha = alpha;
            
            imageZoomView.imageView.layer.borderColor = [[self.borderColor colorWithAlphaComponent:(2-sender.scale)] CGColor];
            
            //sC = 1,5
            // -1 0.5
            
            
            
            CGFloat ratio = sender.scale-1;
            for(UITextField *captionView in self.fullscreenView.captionViews) {
                captionView.alpha = ratio;
            }
        }*/
        /*
        if(pinchRecognizer.state == UIGestureRecognizerStateEnded) {
            if(pinchRecognizer.velocity >= 0) {
                
                CABasicAnimation *borderAlpha = [CABasicAnimation animationWithKeyPath:@"borderColor"];
                // animate from red to blue border ...
                borderAlpha.fromValue = (id)imageZoomView.imageView.layer.borderColor;
                borderAlpha.toValue   = (id)[UIColor clearColor].CGColor;
                borderAlpha.duration = 0.5f;
                borderAlpha.repeatCount = 1;
                [borderAlpha setAutoreverses:NO];
                borderAlpha.removedOnCompletion = NO;
                borderAlpha.fillMode = kCAFillModeForwards;
                [imageZoomView.imageView.layer addAnimation:borderAlpha forKey:@"borderColor"];
                
                [UIView animateWithDuration:0.5f animations:^{
                    imageZoomView.imageView.center = CGPointMake(imageZoomView.frame.size.width/2, imageZoomView.frame.size.height/2);
                    imageZoomView.imageView.transform = CGAffineTransformMakeTranslation(0,0);
                    [self.fullscreenView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1]];
                    self.fullscreenView.closeButton.alpha = 1;
                    for(UITextField *captionView in self.fullscreenView.captionViews) {
                        captionView.alpha = 1;
                    }
                    //imageZoomView.imageView.layer.borderColor = [[UIColor clearColor] CGColor];
                } completion:^(BOOL finished) {
                    imageZoomView.imageView.transform = CGAffineTransformIdentity;
                    imageZoomView.imageView.frame = CGRectMake(0, 0, imageZoomView.frame.size.width, imageZoomView.frame.size.height);
                }];
            }
            else {
                CABasicAnimation *borderAlpha = [CABasicAnimation animationWithKeyPath:@"borderColor"];
                // animate from red to blue border ...
                borderAlpha.fromValue = (id)imageZoomView.imageView.layer.borderColor;
                borderAlpha.toValue   = (id)self.borderColor.CGColor;
                borderAlpha.duration = 0.5f;
                borderAlpha.repeatCount = 1;
                [borderAlpha setAutoreverses:NO];
                borderAlpha.removedOnCompletion = NO;
                borderAlpha.fillMode = kCAFillModeForwards;
                [imageZoomView.imageView.layer addAnimation:borderAlpha forKey:@"borderColor"];
                
                [UIView animateWithDuration:0.5f animations:^{
                    imageZoomView.imageView.center = self.imageViewStartCenter;
                    imageZoomView.imageView.transform = CGAffineTransformMakeScale(ratio.width,ratio.height);
                    [self.fullscreenView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0]];
                    self.fullscreenView.closeButton.alpha = 0;
                } completion:^(BOOL finished) {
                    self.currentGestureTarget.hidden = NO;
                    [self.fullscreenView removeFromSuperview];
                    self.fullscreenView = nil;
                }];
            }
        }*/
        
    }
    
}


-(void) addImage:(UIImage *)image withThumbnail:(UIImage *)thumbnail andCaption:(NSString *)caption
{
    CGFloat y = self.images.count * (400+PADDING_TOP);
    CGFloat x = PADDING_LEFT ;
    
    
    
    [self.images addObject:image];
    [self.thumbnails addObject:thumbnail];
    [self.captions addObject:caption];
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(x, y, 580, 400);
    [button setBackgroundImage:thumbnail forState:UIControlStateNormal];
    button.tag = [self.buttons count];
    
    button.layer.borderWidth = self.borderWidth;
    button.layer.borderColor = [self.borderColor CGColor];
    
    button.layer.shadowColor = [UIColor blackColor].CGColor;
    button.layer.shadowOpacity = 0.8;
    button.layer.shadowRadius = 4;
    button.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    [button.layer setShadowPath:[[UIBezierPath
                                  bezierPathWithRect:button.bounds] CGPath]];
    
    [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttons addObject:button];
    [self.contentView addSubview:button];
    
    UITextView *captionView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 340, 200)];
    captionView.text = caption;
    [captionView setFont:[UIFont systemFontOfSize:16]];
    [self.contentView addSubview:captionView];
    
    CGFloat fixedWidth = captionView.frame.size.width;
    CGSize newSize = [captionView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = captionView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    captionView.frame = CGRectMake(button.frame.origin.x + button.frame.size.width +20, button.frame.origin.y + button.frame.size.height - newFrame.size.height, newFrame.size.width, newFrame.size.height);
    captionView.userInteractionEnabled = NO;
    [captionView setBackgroundColor:[UIColor clearColor]];
    [captionView setTextColor:[UIColor colorWithRed:153.0f/255.0f green:158.0f/255.0f blue:162.0f/255.0f alpha:1]];
    [self.captionViews addObject:captionView];
    //[self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, y + 500)];
    
    CGRect contentViewFrame = self.contentView.frame;
    contentViewFrame.size = CGSizeMake(self.frame.size.width, y+500);
    self.contentView.frame = contentViewFrame;
    
    self.contentSize = CGSizeMake(self.frame.size.width, self.contentView.frame.origin.y + self.contentView.frame.size.height);
}


-(void) addImages:(NSArray *)images withThumbnails:(NSArray *)thumbnails andCaptions:(NSArray *)captions
{
    for(int i = 0; i < images.count; i++) {
        UIImage *image = [images objectAtIndex:i];
        UIImage *thumbnail = [thumbnails objectAtIndex:i];
        NSString *caption = [captions objectAtIndex:i];
        
        [self addImage:image withThumbnail:thumbnail andCaption:caption];
    }
}


-(void) addFullscreenView
{
    self.fullscreenView = [[MKImageGalleryFullscreenView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
    self.fullscreenView.delegate = self;
    [self.fullscreenView setImages:self.images andCaptions:self.captions];
    [self.fullscreenView addGestureRecognizer:self.fullscreenPinchRecognizer];
    [self.fullscreenView addGestureRecognizer:self.fullscreenRotationRecognizer];
    [self.mainView addSubview:self.fullscreenView];
}


-(void) showFullscreenImageWithIndex:(int)index animated:(BOOL)animated
{
    [self addFullscreenView];
    
    if([self.delegate respondsToSelector:@selector(imageGalleryView:willShowFullscreenImageWithIndex:)]) {
        [self.delegate imageGalleryView:self willShowFullscreenImageWithIndex:index];
    }
    
    if(animated) {
        UIButton *button = [self buttonWithIndex:index];
        CGPoint origin = [button.superview convertPoint:button.frame.origin toView:self.mainView];
        [self.fullscreenView showImageWithIndex:index animated:YES fromRect:CGRectMake(origin.x, origin.y, button.frame.size.width, button.frame.size.height) withSourceView:button completion:nil];
    }
    else {
        [self.fullscreenView showImageWithIndex:index animated:NO fromRect:CGRectZero withSourceView: nil completion:nil];
    }
}


-(void) hideFullscreenImageWithIndex:(int)index animated:(BOOL)animated
{
    if([self.delegate respondsToSelector:@selector(imageGalleryView:willHideFullscreenImageWithIndex:)]) {
        [self.delegate imageGalleryView:self willHideFullscreenImageWithIndex:index];
    }
    UIButton *button = [self buttonWithIndex:index];
    CGPoint origin = [button.superview convertPoint:button.frame.origin toView:self.mainView];
    [self.fullscreenView hideImageWithIndex:index animated:YES toRect:CGRectMake(origin.x, origin.y, button.frame.size.width, button.frame.size.height) withSourceView:button completion:^{
        button.hidden = NO;
        self.currentGestureTarget.hidden = NO;
        self.fullscreenView.alpha = 0;
        [self.fullscreenView removeFromSuperview];
        self.fullscreenView = nil;
    }];
}


-(UIButton*) buttonWithIndex:(int) index
{
    for(UIButton *button in self.buttons) {
        if((int) button.tag == index) {
            return button;
        }
    }
    return nil;
}

-(UIButton*) buttonAtLocation: (CGPoint) location
{
    for(UIButton *button in self.buttons) {
        if(button.frame.origin.x < location.x && button.frame.origin.x + button.frame.size.width > location.x && button.frame.origin.y < location.y && button.frame.origin.y + button.frame.size.height > location.y) {
            return button;
        }
    }
    return nil;
}

-(UIView*) mainView
{
    UIWindow *window = self.window;//[[UIApplication sharedApplication] keyWindow];
    return [[window subviews] objectAtIndex:0];
}

-(void) buttonPressed: (UIButton*) button
{
    int index = (int) button.tag;
    [self showFullscreenImageWithIndex:index animated:YES];
}

#pragma mark - DataSource Calling Method (Internal Method)
- (void)getNumberOfElementsFromDataSource {
	SEL dataSourceCall = @selector(numberOfElementsInImageGalleryView:);
	if (self.dataSource && [self.dataSource respondsToSelector:dataSourceCall]) {
		_numberOfImages = [self.dataSource numberOfElementsInImageGalleryView:self];
	} else {
		_numberOfImages = 0;
	}
}



#pragma mark - Data Fetching Methods

-(void) clearData
{
    for(UIView *view in self.captionViews) {
        [view removeFromSuperview];
    }
    
    self.captionViews = [[NSMutableArray alloc] init];
    
    for(UIView *view in self.buttons) {
        [view removeFromSuperview];
    }
    self.buttons = [[NSMutableArray alloc] init];
    
    
    
    self.images = [[NSMutableArray alloc] init];
    self.thumbnails = [[NSMutableArray alloc] init];
    self.captions = [[NSMutableArray alloc] init];
    
}

- (void)reloadData {
	// remove all scrollview subviews and "recycle" them
	[self clearData];
    
	//self.firstVisibleElement = NSIntegerMax;
	//self.lastVisibleElement  = NSIntegerMin;
    
	[self collectData];
    [self refreshContent];
}

- (void)collectData {
	[self getNumberOfElementsFromDataSource];
	/*[self getElementWidthsFromDelegate];
	[self setTotalWidthOfScrollContent];
	[self updateScrollContentInset];
    
	self.dataHasBeenLoaded = YES;*/
	//[self setNeedsLayout];
}

#pragma mark - getters and setters

- (void)setDataSource:(id)newDataSource {
	if (self.dataSource != newDataSource) {
		_dataSource = newDataSource;
		[self reloadData];
	}
}

-(void) setTitle:(NSString *)title
{
    if((title && ![title isEqualToString:@""])) {
        if(!_title || [_title isEqualToString:@""]) {
            self.titleLabel.frame = CGRectMake(PADDING_LEFT, PADDING_TOP, self.frame.size.width-PADDING_LEFT, 50);
            self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, PADDING_TOP+TITLE_PADDING_BOTTOM, self.contentView.frame.size.width, self.contentView.frame.size.height);
            self.contentSize = CGSizeMake(self.frame.size.width, self.contentView.frame.origin.y + self.contentView.frame.size.height);
        }
        self.titleLabel.text = title;
    }
    else if(_title && ![_title isEqualToString:@""]) {
        self.titleLabel.frame = CGRectZero;
        self.contentView.frame = CGRectMake(PADDING_LEFT, TITLE_PADDING_BOTTOM, self.contentView.frame.size.width, self.contentView.frame.size.height);
        
    }
    _title = title;
}

#pragma mark - MKImageGalleryFullscreenView delegate

-(void) fullscreenView:(MKImageGalleryFullscreenView *)fullscreenView closeButtonPressed:(id)sender
{
    self.currentGestureTarget.hidden = NO;
    [self hideFullscreenImageWithIndex:fullscreenView.currentPage animated:YES];
}

-(void) galleryFullscreenView:(MKImageGalleryFullscreenView *)galleryFullscreenView scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([self.delegate respondsToSelector:@selector(imageGalleryView:fullscreenView:scrollViewDidScroll:)]) {
        [self.delegate imageGalleryView:self fullscreenView:galleryFullscreenView scrollViewDidScroll:scrollView];
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
