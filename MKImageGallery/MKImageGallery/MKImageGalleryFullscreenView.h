//
//  MKImageGalleryFullscreenView.h
//  GesturizedImageGallery
//
//  Created by Matteo Koczorek on 20.01.14.
//  Copyright (c) 2014 Matteo Koczorek. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MKImageGalleryFullscreenView;


typedef enum {
    MKImageGalleryModeHorizontal = 0,
    MKImageGalleryModeVertical
} MKImageGalleryMode;

//delegate protocol

@protocol MKImageGalleryFullscreenViewDelegate <NSObject>

@property (nonatomic) MKImageGalleryMode mode;

@optional

-(void) fullscreenView: (MKImageGalleryFullscreenView*) fullscreenView closeButtonPressed: (id) sender;
-(void) galleryFullscreenView:(MKImageGalleryFullscreenView*) galleryFullscreenView scrollViewDidScroll:(UIScrollView*) scrollView;

@end

//interface

@interface MKImageGalleryFullscreenView : UIView <UIScrollViewDelegate>


//properties
@property (nonatomic, weak) id<MKImageGalleryFullscreenViewDelegate> delegate;

@property (nonatomic) int currentPage;

@property (nonatomic, strong) NSArray *images;
@property (nonatomic, strong) NSMutableArray *imageViews;
@property (nonatomic, strong) NSMutableArray *imageZoomViews;
@property (nonatomic, strong) NSMutableArray *captionViews;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, weak) UIView *mainView;
@property (nonatomic, strong) UIButton *previousButton;
@property (nonatomic, strong) UIButton *nextButton;

@property (nonatomic) BOOL infoLayerVisible;



@property (nonatomic, strong) UIPinchGestureRecognizer *pinchRecognizer;
@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;


//methods

-(void) setImages:(NSArray *)images andCaptions: (NSArray*) captions;

-(void) showImageWithIndex: (int) index animated:(BOOL) animated fromRect: (CGRect) rect withSourceView:(UIView*) sourceView completion:(void (^)(void))completion;
-(void) showImageWithIndex:(int)index andZoomValue: (CGFloat) zoomValue fromRect:(CGRect)rect;

-(void) hideImageWithIndex:(int)index animated:(BOOL)animated toRect:(CGRect)rect withSourceView:(UIView*) sourceView completion:(void (^)(void))completion;
-(void) hideImageWithIndex:(int)index andZoomValue: (CGFloat) zoomValue toRect:(CGRect)rect;


@end




