//
//  MKImageGalleryFullscreenView.m
//  GesturizedImageGallery
//
//  Created by Matteo Koczorek on 20.01.14.
//  Copyright (c) 2014 Matteo Koczorek. All rights reserved.
//

#define ANIMATION_DURATION 0.3

#import "MKImageGalleryFullscreenView.h"
#import "MKImageZoomView.h"

#define MARGIN 20

@implementation MKImageGalleryFullscreenView

@synthesize images = _images;
@synthesize currentPage = _currentPage;
@synthesize mainView = _mainView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self construct];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        [self construct];
    }
    return self;
}

-(void) construct
{
    
    self.imageViews = [[NSMutableArray alloc] init];
    self.imageZoomViews = [[NSMutableArray alloc] init];
    self.captionViews = [[NSMutableArray alloc] init];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(-MARGIN, -MARGIN, self.frame.size.width+MARGIN*2, self.frame.size.height + MARGIN*2)];
    self.scrollView.pagingEnabled = YES;
    self.scrollView.delegate = self;
    [self addSubview:self.scrollView];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.closeButton.frame = CGRectMake(self.frame.size.width-170, 20, 155, 60);
    
    self.previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.previousButton.frame = CGRectMake(self.frame.size.width / 2 - 22, 20, 44, 44);
    [self.previousButton setBackgroundImage:[UIImage imageNamed:@"ArrowUp"] forState:UIControlStateNormal];
    [self.previousButton setBackgroundImage:[UIImage imageNamed:@"ArrowUp_Active"] forState:UIControlStateHighlighted];
    [self addSubview:self.previousButton];
    [self.previousButton addTarget:self action:@selector(previousImage) forControlEvents:UIControlEventTouchUpInside];
    self.previousButton.alpha = 0;
    
    self.nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.nextButton.frame = CGRectMake(self.frame.size.width / 2 - 22, self.frame.size.height - 44 - 20, 44, 44);
    [self.nextButton setBackgroundImage:[UIImage imageNamed:@"ArrowDown"] forState:UIControlStateNormal];
    [self.nextButton setBackgroundImage:[UIImage imageNamed:@"ArrowDown_Active"] forState:UIControlStateHighlighted];
    [self addSubview:self.nextButton];
    [self.nextButton addTarget:self action:@selector(nextImage) forControlEvents:UIControlEventTouchUpInside];
    self.nextButton.alpha = 0;
    
    NSString *closeImageName = @"CloseButton_de@2x.png";
    [self.closeButton setBackgroundImage:[UIImage imageNamed:closeImageName] forState:UIControlStateNormal];
    NSString *closeImageNameActive = @"CloseButtonActive_de@2x.png";
    [self.closeButton setBackgroundImage:[UIImage imageNamed:closeImageNameActive] forState:UIControlStateHighlighted];
    
    [self.closeButton addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.closeButton];
    self.infoLayerVisible = YES;
    self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognized)];
    [self addGestureRecognizer:self.tapRecognizer];
}

-(void) nextImage
{
    [UIView animateWithDuration:0.5f animations:^{
       self.currentPage++;
    }];
}

-(void) previousImage
{
    [UIView animateWithDuration:0.5f animations:^{
        self.currentPage--;
    }];
}

-(void) tapRecognized
{
    NSLog(@"tap");
    
    self.infoLayerVisible = !self.infoLayerVisible;
    
    [UIView animateWithDuration:0.3f animations:^{
            for(UITextView *captionView in self.captionViews) {
                if(!self.infoLayerVisible) {
                    captionView.alpha = 0;
                }
                else {
                    captionView.alpha = 1;
                }
            }
        if(!self.infoLayerVisible) {
            //self.closeButton.alpha = 0;
            self.nextButton.alpha = 0;
            self.previousButton.alpha = 0;
        }
        else {
            if(self.currentPage != 0) {
                self.previousButton.alpha = 1;
            }
            if(self.currentPage < self.images.count - 1) {
                self.nextButton.alpha = 1;
            }
            //self.closeButton.alpha = 1;
        }
    }];
}

-(void) showImageWithIndex: (int) index animated:(BOOL) animated fromRect: (CGRect) rect withSourceView:(UIView*) sourceView completion:(void (^)(void))completion
{
    self.currentPage = index;
    if(animated) {
        MKImageZoomView *imageZoomView = [self.imageZoomViews objectAtIndex:index];
        if(!CGRectIsNull(rect)) {
            imageZoomView.imageView.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
        }
        if(sourceView) {
            CGFloat ratio = imageZoomView.imageView.frame.size.width / sourceView.frame.size.width;
            imageZoomView.imageView.layer.borderWidth = sourceView.layer.borderWidth;
            imageZoomView.imageView.layer.borderColor = sourceView.layer.borderColor;
            
            imageZoomView.imageView.layer.shadowColor = [UIColor blackColor].CGColor;
            imageZoomView.imageView.layer.shadowOpacity = 0.8;
            imageZoomView.imageView.layer.shadowRadius = 4;
            imageZoomView.imageView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
            [imageZoomView.imageView.layer setShadowPath:[[UIBezierPath bezierPathWithRect:imageZoomView.imageView.bounds] CGPath]];
        }
        
        /*CABasicAnimation* borderAnimation = [CABasicAnimation animationWithKeyPath:@"borderWidth"];
        [borderAnimation setFromValue:[NSNumber numberWithFloat:sourceView.layer.borderWidth]];
        [borderAnimation setToValue:[NSNumber numberWithFloat:0.0f]];
        [borderAnimation setRepeatCount:1.0];
        [borderAnimation setAutoreverses:NO];
        [borderAnimation setDuration:0.5f];
        borderAnimation.removedOnCompletion = NO;
        borderAnimation.fillMode = kCAFillModeForwards;
        
        [imageZoomView.imageView.layer addAnimation:borderAnimation forKey:@"animateBorder"];*/
        CABasicAnimation *borderAlpha = [CABasicAnimation animationWithKeyPath:@"borderColor"];
        // animate from red to blue border ...
        borderAlpha.fromValue = (id)imageZoomView.imageView.layer.borderColor;
        borderAlpha.toValue   = (id)[UIColor clearColor].CGColor;
        borderAlpha.duration = ANIMATION_DURATION;
        borderAlpha.repeatCount = 1;
        [borderAlpha setAutoreverses:NO];
        borderAlpha.removedOnCompletion = NO;
        borderAlpha.fillMode = kCAFillModeForwards;
        [imageZoomView.imageView.layer addAnimation:borderAlpha forKey:@"borderColor"];
        
        CGRect finalFrame = CGRectMake(0, 0, self.scrollView.frame.size.width - MARGIN*2, self.scrollView.frame.size.height - MARGIN*2);
        
        CABasicAnimation *shadowPathAnimation = [CABasicAnimation animationWithKeyPath:@"shadowPath"];
        shadowPathAnimation.fromValue = (id)imageZoomView.imageView.layer.shadowPath;
        shadowPathAnimation.toValue = (id) [[UIBezierPath bezierPathWithRect:finalFrame] CGPath];
        shadowPathAnimation.duration = ANIMATION_DURATION;
        shadowPathAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        [imageZoomView.imageView.layer addAnimation:shadowPathAnimation forKey:@"shadowPath"];
        
        [UIView animateWithDuration:ANIMATION_DURATION delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            imageZoomView.imageView.frame = finalFrame;
            [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1]];
            self.closeButton.alpha = 1;
            //imageView.imageView.layer.borderColor = [[UIColor clearColor] CGColor];
            //imageView.imageView.layer.shadowOpacity = 0;
            //imageView.imageView.transform = CGAffineTransformIdentity;
            for(UITextView *captionView in self.captionViews) {
                
                captionView.alpha = 1;
            }
            if(self.currentPage > 0) {
                self.previousButton.alpha = 1;
            }
            if(self.currentPage < self.images.count) {
                self.nextButton.alpha = 1;
            }
        }completion:^(BOOL finished){
            if(completion) {
                completion();
            }
        }];
    }
}

-(void) hideImageWithIndex:(int)index animated:(BOOL)animated toRect:(CGRect)rect withSourceView:(UIView*) sourceView completion:(void (^)(void))completion
{
    if(animated) {
        MKImageZoomView *imageView = [self.imageZoomViews objectAtIndex:index];
        
        
        
        if(sourceView) {
            imageView.imageView.layer.borderColor = [[UIColor clearColor] CGColor];
            imageView.imageView.layer.borderWidth = sourceView.layer.borderWidth;
            
            /*imageView.imageView.layer.shadowColor = sourceView.layer.shadowColor;
            imageView.imageView.layer.shadowOffset = sourceView.layer.shadowOffset;
            imageView.imageView.layer.shadowOpacity = sourceView.layer.shadowOpacity;
            imageView.imageView.layer.shadowRadius = sourceView.layer.shadowRadius;
            imageView.imageView.layer.shadowPath = [[UIBezierPath bezierPathWithRect:imageView.imageView.frame] CGPath];*/
            
            CABasicAnimation *borderTransparency = [CABasicAnimation animationWithKeyPath:@"borderColor"];
            borderTransparency.fromValue = (id) imageView.imageView.layer.borderColor;
            borderTransparency.toValue = (id) sourceView.layer.borderColor;
            borderTransparency.duration = ANIMATION_DURATION;
            borderTransparency.autoreverses = NO;
            borderTransparency.removedOnCompletion = NO;
            borderTransparency.fillMode = kCAFillModeForwards;
            
            borderTransparency.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
            [imageView.imageView.layer addAnimation:borderTransparency forKey:@"borderColor"];
            /*CABasicAnimation* borderAnimation = [CABasicAnimation animationWithKeyPath:@"borderWidth"];
            [borderAnimation setFromValue:[NSNumber numberWithFloat:0.0f]];
            [borderAnimation setToValue:[NSNumber numberWithFloat:sourceView.layer.borderWidth]];
            [borderAnimation setRepeatCount:1.0];
            [borderAnimation setAutoreverses:NO];
            [borderAnimation setDuration:0.5f];
            [imageView.imageView.layer addAnimation:borderAnimation forKey:@"animateBorder"];*/
            
            
            
            /*CGRect shadowTarget = CGRectMake(0, 0, 200, 200);
            CABasicAnimation *shadowPathAnimation = [CABasicAnimation animationWithKeyPath:@"shadowPath"];
            shadowPathAnimation.fromValue = (id)imageView.imageView.layer.shadowPath;
            shadowPathAnimation.toValue = (id) [[UIBezierPath bezierPathWithRect:CGRectMake(0, 0, rect.size.width, rect.size.height)] CGPath];
            shadowPathAnimation.duration = 0.5f;
            shadowPathAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
            [imageView.imageView.layer addAnimation:shadowPathAnimation forKey:@"shadowPath"];*/
        }
        
        
        
        [UIView animateWithDuration:ANIMATION_DURATION delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0]];
            self.closeButton.alpha = 0;
            imageView.imageView.frame = rect;
            
            for(UITextView *captionView in self.captionViews) {
                captionView.alpha = 0;
            }
            self.previousButton.alpha = 0;
            self.nextButton.alpha = 0;
        } completion:^(BOOL finished) {
            if(completion) {
                completion();
            }
        }];
    }
}

-(void) showImageWithIndex:(int)index andZoomValue:(CGFloat)zoomValue fromRect:(CGRect)rect
{
    self.currentPage = index;
    MKImageZoomView *imageView = [self.imageZoomViews objectAtIndex:index];
    CGFloat mod = (1 - zoomValue/8);
    if(mod < 0) {
        mod = mod*0.4;
    }
    if(mod > 1) {
        mod = 1;
    }
    imageView.imageView.frame = CGRectMake(rect.origin.x * mod, rect.origin.y * mod, rect.size.width + (self.scrollView.frame.size.width - rect.size.width) * (1-mod), rect.size.height + (self.scrollView.frame.size.height - rect.size.height) * (1-mod));
}

-(void) hideImageWithIndex:(int)index andZoomValue:(CGFloat)zoomValue toRect:(CGRect)rect
{
    self.currentPage = index;
    MKImageZoomView *imageView = [self.imageZoomViews objectAtIndex:index];
    CGFloat mod = - zoomValue * 1.5;
    if(mod < 0) {
        mod = mod*0.4;
    }
    if(mod > 1) {
        mod = 1;
    }
    imageView.imageView.frame = CGRectMake(rect.origin.x * mod, rect.origin.y * mod, rect.size.width + (self.scrollView.frame.size.width - rect.size.width) * (1-mod), rect.size.height + (self.scrollView.frame.size.height - rect.size.height) * (1-mod));
}

-(UIView*) mainView
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    return [[window subviews] objectAtIndex:0];
}

-(void) setCurrentPage:(int)currentPage
{
    if(currentPage >= 0 && currentPage < self.images.count) {
        _currentPage = currentPage;
        CGPoint contentOffset;
        if([self.delegate mode] == MKImageGalleryModeHorizontal) {
            contentOffset = CGPointMake(currentPage*self.scrollView.frame.size.width, 0);
        }
        else if([self.delegate mode] == MKImageGalleryModeVertical) {
            contentOffset = CGPointMake(0,currentPage*self.scrollView.frame.size.height);
        }
        [self.scrollView setContentOffset:contentOffset];
        
    }
    
    if(currentPage == 0) {
        [UIView animateWithDuration:0.3f animations:^{
            self.previousButton.alpha = 0;
        }];
    }
    else if(currentPage >= self.images.count-1) {
        [UIView animateWithDuration:0.3f animations:^{
            self.nextButton.alpha = 0;
        }];
    }
    else {
        
        [UIView animateWithDuration:0.3f animations:^{
            self.nextButton.alpha = 1;
            self.previousButton.alpha = 1;
        }];
    }
}

-(void) closeButtonPressed: (id) sender
{
    if([self.delegate respondsToSelector:@selector(fullscreenView:closeButtonPressed:)]) {
        [self.delegate fullscreenView:self closeButtonPressed:sender];
    }
}

-(void) setImages:(NSArray *)images andCaptions:(NSArray *)captions
{
    NSLog(@"galleryMode: %@", self.delegate);
    _images = images;
    for(int i = 0; i < images.count; i++) {
        
        NSString *caption = [captions objectAtIndex:i];
        
        CGRect rect;
        if([self.delegate mode] == MKImageGalleryModeHorizontal) {
            rect = CGRectMake((i*self.scrollView.frame.size.width+MARGIN), MARGIN, self.scrollView.frame.size.width-MARGIN*2, self.scrollView.frame.size.height -MARGIN*2);
        }
        else if([self.delegate mode] == MKImageGalleryModeVertical) {
            rect = CGRectMake((MARGIN), i*self.scrollView.frame.size.height+MARGIN, self.scrollView.frame.size.width-MARGIN*2, self.scrollView.frame.size.height -MARGIN*2);
        }
        MKImageZoomView *imageZoomView = [[MKImageZoomView alloc] initWithFrame:rect andImage:[images objectAtIndex:i]];
        //imageZoomView.userInteractionEnabled = NO;
        
        
        //imageView.image = [images objectAtIndex:i];
        [self.imageZoomViews addObject:imageZoomView];
        CGSize contentSize;
        if([self.delegate mode] == MKImageGalleryModeHorizontal) {
            contentSize = CGSizeMake((self.scrollView.frame.size.width)*(i+1), self.scrollView.frame.size.height);
        }
        else if([self.delegate mode] == MKImageGalleryModeVertical) {
            contentSize = CGSizeMake(self.scrollView.frame.size.width, self.scrollView.frame.size.height*(i+1));
        }
        [self.scrollView setContentSize:contentSize];
        [self.scrollView addSubview:imageZoomView];
        
        UITextView *captionView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 280, 200)];
        captionView.text = caption;
        captionView.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10);
        captionView.userInteractionEnabled = NO;
        [captionView setFont:[UIFont systemFontOfSize:16]];
        captionView.alpha = 0;
        [self.captionViews addObject:captionView];
        [self.scrollView addSubview:captionView];
        
        CGFloat fixedWidth = captionView.frame.size.width;
        CGSize newSize = [captionView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
        CGRect newFrame = captionView.frame;
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
        captionView.frame = CGRectMake(1024-280, imageZoomView.frame.origin.y + imageZoomView.frame.size.height - newFrame.size.height - 20, newFrame.size.width, newFrame.size.height);
        [captionView setBackgroundColor:[UIColor colorWithRed:26/255 green:26/255 blue:26/255 alpha:0.8]];
        [captionView setTextColor:[UIColor whiteColor]];
    }
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    self.scrollView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
}

#pragma mark - ScrollView Delegate

-(void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    _currentPage = [self.delegate mode] == MKImageGalleryModeHorizontal ? (int) roundf(scrollView.contentOffset.x / scrollView.frame.size.width) : (int) roundf(scrollView.contentOffset.y / scrollView.frame.size.height);
    
    if([self.delegate respondsToSelector:@selector(galleryFullscreenView:scrollViewDidScroll:)]) {
        [self.delegate galleryFullscreenView:self scrollViewDidScroll:scrollView];
    }
}

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(self.infoLayerVisible) {
    if(self.currentPage == 0) {
        [UIView animateWithDuration:0.3f animations:^{
            self.previousButton.alpha = 0;
        }];
    }
    else if(self.currentPage >= self.images.count-1) {
        [UIView animateWithDuration:0.3f animations:^{
            self.nextButton.alpha = 0;
        }];
    }
    else {
        
        [UIView animateWithDuration:0.3f animations:^{
            self.nextButton.alpha = 1;
            self.previousButton.alpha = 1;
        }];
    }
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
