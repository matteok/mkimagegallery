//
//  MKImageGalleryScrollView.m
//  küste im blick
//
//  Created by Matteo Koczorek on 09.04.14.
//  Copyright (c) 2014 Kite. All rights reserved.
//

#import "MKImageGalleryScrollView.h"

@implementation MKImageGalleryScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
    }
    return self;
}

-(BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
