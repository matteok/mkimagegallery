//
//  ImageZoomView.m
//  COSYNA
//
//  Created by Matteo Koczorek on 05.02.13.
//  Copyright (c) 2013 Matteo Koczorek. All rights reserved.
//

#import "MKImageZoomView.h"
#import "MKImageGalleryScrollView.h"

@implementation MKImageZoomView

@synthesize imageView = _imageView;
@synthesize scrollView = _scrollView;
@synthesize image = _image;
@synthesize contentView = _contentView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(id) initWithFrame:(CGRect)frame andImage:(UIImage *)image {
    self = [super initWithFrame:frame];
    if (self) {
        //self.frame = frame;
        self.image = image;
        [self construct];
    }
    return self;
}

-(void) construct
{
    self.scrollView = [[MKImageGalleryScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    
    self.scrollView.delegate = self;
    [self.scrollView setClipsToBounds:YES];
    
    [self.scrollView setContentSize:CGSizeMake(self.frame.size.width, self.frame.size.height)];
    
    self.imageView = [[UIImageView alloc] init];
    self.imageView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    self.imageView.image = self.image;
    
    
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    
    [self.contentView addSubview:self.imageView];
    
    [self.scrollView addSubview:self.contentView];
    self.scrollView.bouncesZoom = NO;
    self.scrollView.minimumZoomScale = 1.0;
    self.scrollView.maximumZoomScale = 4.0;
    
    
    [self addSubview:self.scrollView];
}

-(UIView*) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.contentView;
}

-(void) scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view
{
    
}

-(void) scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    if(scrollView.zoomScale <= 1) scrollView.bouncesZoom = NO;
    else scrollView.bouncesZoom = YES;
}


-(void) zoomOut
{
    
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    /*self.imageView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    self.scrollView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    self.contentView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);*/
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    
    //[self.delegate imageZoomViewTouched];
   // NSLog(@"Tocuhes Ended imageZoom");
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
